public class Main{
	private int x;

	public void setX(int x){
		this.x = x;
	}

	public void foo(Main other){
		System.out.println(this.x);
		System.out.println(other.x);
	}

	public static void main(String[] args) {
		Main a1 = new Main();
		Main a2 = new Main();

		a1.setX(5);			//		A.setX(5, a1);
		a2.setX(10);		//		A.setX(10, a2);

		a1.foo();			//		A.foo(a1);
		a2.foo();			//		A.foo(a2);

		a1 = this;

		a1.x;
		this.x;
		
	}
}